function compute(data) {
    let cleanData = data.split('\n')
    let result = []
    for(let i of cleanData){
        if(i !== ""){
            let singleData = i.split(" ")
            let id = parseInt(singleData[0])
            let nom = singleData[1].split('-')[0]
            let reste = singleData.slice(2, singleData.length+1).toString().replaceAll(',,', ',')
            result.push([id, JSON.parse(reste)])
        }
    }
    return result
}
    
